## Synopsis

Scaffolder is a Visual Studio 2012-2015 solution for producing templated stubs for projects which are highly structured. If you want to contribute, or if you have other questions, I'm @OutOfBoundsExc on twitter.

## Code Example 

The following Scaffold.txt creates 1 file for each sub folder from where it sits. This file is named after the folder in which it sits, followed by "Controller.cs". Each Scaffold.txt can contain as many file, folder or depth directives as needed.

	%%%depth:>0
	%%%file:{{folder}}Controller.cs
	namespace MyProject.Controllers
	{
		public class {{folder}}Controller
		{

		}
	}

## Installation

1. Clone the repository.
1. Copy the ScaffoldProgram project into your solution
1. In a folder, place a file whos name ends in Scaffold.txt
1. If you need to adjust the number of folders above itself into which the ScaffoldProgram searches, adjust the number in the post-commit hook of ScaffoldProgram's csproj file.
1. Start making some stubs or samples in one or more Scaffold.txt documents.

## License

Copyright (c) 2017 Jason Cole

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.