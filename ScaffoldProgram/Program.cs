using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScaffoldProgram
{
    class Program
    {
        static int Main(string[] args)
        {
            var loc = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;


            var iter = 4;

            if (string.Join("", args).Length > 0)
            {
                iter = int.Parse(args[0].Trim()) + 1;
            }
            while (iter-- > 0)
            {
                loc = System.IO.Path.GetDirectoryName(loc);
            }

            Trunk t = new Trunk(loc);
            //Console.ReadKey();

            return 0;
        }
    }
}
