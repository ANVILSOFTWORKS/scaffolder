using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScaffoldProgram
{
    public class Trunk
    {
        public Trunk(string Path)
        {
            Path = Path.Replace("file:\\", "");

            // First, find all the relevant files
            var AllConfigPaths = Functions.ProcessDirectory(Path).Where(f => f.ToLower().EndsWith("scaffold.txt"));
            var AllConfigs = AllConfigPaths.Select(j => Functions.ReadFile(j));

            // Then Parse each file's "rules" in order (default depth is >0)
            var ParsedFiles = AllConfigPaths.Zip(AllConfigs, (path, content) => new ScaffoldFile(path, content)).ToList();

            ParsedFiles.ForEach(f =>
                {
                    f.Rules
                        .Where(r => r is FolderRule)
                        .ToList()
                        .ForEach(r =>
                        {
                            var dirs = Functions.GetSubDirectory(r.RootPath, r.DepthCount, r.DepthSymbol);

                            dirs.ForEach(d =>
                            {
                                // resolve {{Folder}}
                                var instance = r.Clone();

                                //warning, the following is destructive, and in-place
                                instance.ApplyMustaches(d);

                                if (!Directory.Exists(d + "\\" + instance.FileName))
                                {
                                    System.IO.Directory.CreateDirectory(d + "\\" + instance.FileName);

                                    var proj = Functions.GetNearestFile(d + "\\" + instance.FileName, "proj");

                                    var text = Functions.ReadFile(proj);

                                    var insert = text.IndexOf("<Compile Include");

                                    var relative = d.Substring(System.IO.Path.GetDirectoryName(proj).Length + 1)
                                        + "\\" + instance.FileName;

                                    relative = "<Folder Include=\"" + relative + "\" />" + "\n";

                                    text = text.Insert(insert, relative);

                                    File.WriteAllText(proj, text);
                                }
                            });
                        }
                    );
                    f.Rules
                        .Where(r => !(r is FolderRule))
                        .ToList()
                        .ForEach(r =>
                        {
                            // parse the rule for each 'file-to-be'
                            var dirs = Functions.GetSubDirectory(r.RootPath, r.DepthCount, r.DepthSymbol);

                            dirs.ForEach(d =>
                                {
                                    // resolve {{Folder}}
                                    var instance = r.Clone();

                                    //warning, the following is destructive, and in-place
                                    instance.ApplyMustaches(d);


                                    if (!File.Exists(d + "\\" + instance.FileName))
                                    {
                                        // Next, put the contents into the file
                                        File.WriteAllText(d + "\\" + instance.FileName, instance.FileStub);

                                        // Next, write lines as follows in the .csproj file
                                        //<Compile Include="Properties\AssemblyInfo.cs" />
                                        var proj = Functions.GetNearestFile(d + "\\" + instance.FileName, "proj");

                                        var text = Functions.ReadFile(proj);

                                        var insert = text.IndexOf("<Compile Include");

                                        var relative = d.Substring(System.IO.Path.GetDirectoryName(proj).Length + 1)
                                            + "\\" + instance.FileName;

                                        relative = "<" + Functions.FileToAction(relative) + " Include=\"" + relative + "\" />" + "\n";

                                        text = text.Insert(insert, relative);

                                        File.WriteAllText(proj, text);

                                        // TODO: get the below working, as it's much nicer...

                                        // var p = Microsoft.Build.Evaluation
                                        //     .ProjectCollection.GlobalProjectCollection
                                        //     .LoadedProjects.FirstOrDefault(pr => pr.FullPath == proj);
                                        //
                                        // if (p == null)
                                        // {
                                        //     p = new Microsoft.Build.Evaluation.Project(proj);
                                        // }
                                        // p.AddItem("Folder", @"C:\projects\BabDb\test\test2");
                                        // p.AddItem("Compile", @"C:\projects\BabDb\test\test2\Class1.cs");
                                        // p.Save();
                                    }
                                });
                        });

                    //f.Rules.ForEach(r => Console.WriteLine(r.FileName + "\n------------\n" + r.FileStub));
                });


            // OPTIONAL BULLSHIT
            // think about resolving {{date}} and {{GrandFolder}}
            // consider parsing {{Folders}}, which repeats lines on which it appears for each folder

        }
    }
}
