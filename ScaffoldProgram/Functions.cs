using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScaffoldProgram
{
    public static class Functions
    {
        public static string FileToAction(string file)
        {
            try
            {
                file = file.Split('.').Last().ToLower();

                switch (file)
                {
                    case "cs":
                    case "fs":
                    case "cpp":
                    case "hpp":
                    case "h":
                    case "c":
                    case "vb":
                        return "Compile";

                    case "xaml":
                        return "Page";

                    case "ts":
                        return "TypeScriptCompile";

                    case "html":
                    case "htm":
                    case "js":
                    case "xml":
                    case "css":
                    case "asp":
                    case "aspx":
                        return "Content";

                    case "conf":
                    case "cfg":
                    case "config":
                    case "ini":
                    case "json":
                    case "scss":
                    case "less":
                    case "txt":
                    case "tt":
                    case "log":
                    case "cshtml":
                    case "diagram":
                    case "doc":
                    case "xls":
                    case "docx":
                    case "xlsx":
                    case "sql":
                        return "None";
                }
            }
            catch
            {

            }
            return "None";
        }
        public static string ReadFile(string file)
        {
            return File.ReadAllText(file);
        }

        public static string GetNearestFile(string targetPath, string extension)
        {
            try
            {
                var targetDirectory = Path.GetDirectoryName(targetPath);

                string[] fileEntries = Directory.GetFiles(targetDirectory);

                foreach (string fileName in fileEntries)
                {
                    if (fileName.EndsWith(extension))
                    {
                        return fileName;
                    }
                }
                return GetNearestFile(targetDirectory, extension);
            }
            catch
            {
                return "";
            }

        }

        public static List<string> GetSubDirectory(string targetDirectory, int depth, char depthCharacter = '>', int currentDepth = 0)
        {
            try
            {
                var toRet = new List<string>();

                if ((currentDepth == depth && depthCharacter == '=')
                || (currentDepth > depth && depthCharacter == '>')
                || (currentDepth < depth && depthCharacter == '<'))
                {
                    toRet.Add(targetDirectory);
                }

                // Recurse into subdirectories of this directory.
                string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
                foreach (string subdirectory in subdirectoryEntries)
                {
                    toRet.AddRange(GetSubDirectory(subdirectory, depth, depthCharacter, currentDepth + 1));
                }
                return toRet;
            }
            catch
            {
                return new List<string>();
            }
        }

        // https://msdn.microsoft.com/en-us/library/07wt70x2(v=vs.110).aspx
        // Process all files in the directory passed in, recurse on any directories 
        // that are found, and process the files they contain.
        public static List<string> ProcessDirectory(string targetDirectory)
        {
            // Return list
            var toRet = new List<string>();

            try
            {
                // Process the list of files found in the directory.
                string[] fileEntries = Directory.GetFiles(targetDirectory);
                foreach (string fileName in fileEntries)
                    toRet.Add(fileName);

                // Recurse into subdirectories of this directory.
                string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
                foreach (string subdirectory in subdirectoryEntries)
                {
                    toRet.AddRange(ProcessDirectory(subdirectory));
                }
            }
            catch (System.UnauthorizedAccessException e)
            {
            }
            return toRet;
        }

        public static string ReplaceWhenNotWithin(this string main, string find, string replaceWith, string butNot)
        {
            var i = main.Split(new string[] { butNot }, StringSplitOptions.None);

            i = i.Select(x => x.Replace(find, replaceWith)).ToArray();

            return string.Join(butNot, i.ToArray());
        }

        public static string RepToken(this string main, string token, string replaceWith)
        {
            var highToken = "{{" + token[0].ToString().ToUpper() + token.Substring(1) + "}}";
            var lowToken = "{{" + token[0].ToString().ToLower() + token.Substring(1) + "}}";

            return main.Trim()
                .ReplaceWhenNotWithin(highToken, replaceWith, "{" + highToken + "}")
                .ReplaceWhenNotWithin(lowToken, replaceWith, "{" + lowToken + "}")
                .Replace("{" + highToken + "}", highToken)
                .Replace("{" + lowToken + "}", lowToken);
        }
    }
}
