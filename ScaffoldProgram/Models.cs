using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScaffoldProgram
{

    public class ScaffoldFile
    {
        public List<Rule> Rules = new List<Rule>();
        public string FilePath = "";
        public string FileContents = "";

        public ScaffoldFile(string filePath, string fileContents)
        {
            FilePath = filePath;
            FileContents = fileContents;
            Parse();
        }

        public void Parse()
        {
            Rules.Clear();

            char depthSymbol = '>';
            int depthCount = 0;

            char cacheDepthSymbol = '>';
            int cacheDepthCount = 0;

            string name = "";
            bool firstFile = true;

            StringBuilder buffer = new StringBuilder();


            FileContents.Split('\n').ToList().ForEach(line =>
            {
                if (line.ToLower().StartsWith("%%%depth:"))
                {
                    depthSymbol = line.Substring(9).Trim()[0];
                    depthCount = int.Parse(line.Substring(9).Trim().Substring(1));
                }
                else if (line.ToLower().StartsWith("%%%folder:"))
                {
                    var fold = line.Substring(10);
                    Rules.Add(new FolderRule()
                    {
                        DepthCount = cacheDepthCount,
                        DepthSymbol = cacheDepthSymbol,
                        FileName = fold,
                        FileStub = "",
                        RootPath = FilePath,
                    });
                }
                else if (line.ToLower().StartsWith("%%%file:"))
                {
                    if (firstFile)
                    {
                        firstFile = false;
                    }
                    else if (name.Length > 0)
                    {
                        Rules.Add(new Rule()
                        {
                            DepthCount = cacheDepthCount,
                            DepthSymbol = cacheDepthSymbol,
                            FileName = name,
                            FileStub = buffer.ToString(),
                            RootPath = FilePath,
                        });
                        //reset for the next one.
                        buffer.Clear();
                    }
                    name = line.Substring(8).Trim();
                    cacheDepthCount = depthCount;
                    cacheDepthSymbol = depthSymbol;
                }
                else if (line.ToLower().StartsWith("%%%%"))
                {
                    buffer.Append("\n" + line.Substring(1));
                }
                else
                {
                    buffer.Append("\n" + line);
                }
            });
            Rules.Add(new Rule()
            {
                DepthCount = depthCount,
                DepthSymbol = depthSymbol,
                FileName = name,
                FileStub = buffer.ToString(),
                RootPath = FilePath,
            });

        }
    }

    public class Rule
    {
        public int DepthCount = 0;
        public char DepthSymbol = '>';
        public string FileStub = "";
        public string FileName = "";

        private string _RootPath = "";
        public string RootPath
        {
            get
            {
                return _RootPath;
            }
            set
            {
                var v = value.Split('\\').ToList();
                v.RemoveAt(v.Count - 1);
                _RootPath = string.Join("\\", v);
            }
        }

        public Rule Clone()
        {
            return new Rule()
            {
                DepthCount = DepthCount,
                DepthSymbol = DepthSymbol,
                FileStub = FileStub,
                FileName = FileName,
                _RootPath = _RootPath,
            };
        }
        public void ApplyMustaches(string folderValue)
        {
            var folder = folderValue.Split('\\').Last();

            FileName = FileName.RepToken("folder", folder);

            Console.WriteLine("Folder" + folderValue);

            FileStub = FileStub.RepToken("folder", folder);
        }
    }

    public class FolderRule : Rule
    {

    }
}
